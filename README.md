ics-ans-role-junos-banner
=========================

Ansible role to install junos-banner, which sets the login banner on Junos switches.

Requirements
------------

- ansible >= 2.5
- molecule >= 2.6
- ncclient

Role Variables
--------------

```yaml
junos_banner: "#Unauthorized access to this device is prohibited!#"

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-banner
```

License
-------

BSD 2-clause
